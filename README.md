## GitLab Idea To Production Demo

This demo runs in OpenShift Origin and shows an early glance at the features that make up our Idea to Production product direction.
For more information see here: https://about.gitlab.com/direction/#scope

## GitLab Internal

Checkout the onramp document to use these templates in our internal cluster:
https://docs.google.com/a/gitlab.com/document/d/1rae3AVoXpxEwjJ33YH_eseOfhCBLs6g0k1aXuEIRWFE/edit?usp=sharing

## License

MIT License
